# BambangShop Publisher App
Tutorial and Example for Advanced Programming 2024 - Faculty of Computer Science, Universitas Indonesia

---

## About this Project
In this repository, we have provided you a REST (REpresentational State Transfer) API project using Rocket web framework.

This project consists of four modules:
1.  `controller`: this module contains handler functions used to receive request and send responses.
    In Model-View-Controller (MVC) pattern, this is the Controller part.
2.  `model`: this module contains structs that serve as data containers.
    In MVC pattern, this is the Model part.
3.  `service`: this module contains structs with business logic methods.
    In MVC pattern, this is also the Model part.
4.  `repository`: this module contains structs that serve as databases and methods to access the databases.
    You can use methods of the struct to get list of objects, or operating an object (create, read, update, delete).

This repository provides a basic functionality that makes BambangShop work: ability to create, read, and delete `Product`s.
This repository already contains a functioning `Product` model, repository, service, and controllers that you can try right away.

As this is an Observer Design Pattern tutorial repository, you need to implement another feature: `Notification`.
This feature will notify creation, promotion, and deletion of a product, to external subscribers that are interested of a certain product type.
The subscribers are another Rocket instances, so the notification will be sent using HTTP POST request to each subscriber's `receive notification` address.

## API Documentations

You can download the Postman Collection JSON here: https://ristek.link/AdvProgWeek7Postman

After you download the Postman Collection, you can try the endpoints inside "BambangShop Publisher" folder.
This Postman collection also contains endpoints that you need to implement later on (the `Notification` feature).

Postman is an installable client that you can use to test web endpoints using HTTP request.
You can also make automated functional testing scripts for REST API projects using this client.
You can install Postman via this website: https://www.postman.com/downloads/

## How to Run in Development Environment
1.  Set up environment variables first by creating `.env` file.
    Here is the example of `.env` file:
    ```bash
    APP_INSTANCE_ROOT_URL="http://localhost:8000"
    ```
    Here are the details of each environment variable:
    | variable              | type   | description                                                |
    |-----------------------|--------|------------------------------------------------------------|
    | APP_INSTANCE_ROOT_URL | string | URL address where this publisher instance can be accessed. |
2.  Use `cargo run` to run this app.
    (You might want to use `cargo check` if you only need to verify your work without running the app.)

## Mandatory Checklists (Publisher)
-   [ ] Clone https://gitlab.com/ichlaffterlalu/bambangshop to a new repository.
-   **STAGE 1: Implement models and repositories**
    -   [ ] Commit: `Create Subscriber model struct.`
    -   [ ] Commit: `Create Notification model struct.`
    -   [ ] Commit: `Create Subscriber database and Subscriber repository struct skeleton.`
    -   [ ] Commit: `Implement add function in Subscriber repository.`
    -   [ ] Commit: `Implement list_all function in Subscriber repository.`
    -   [ ] Commit: `Implement delete function in Subscriber repository.`
    -   [ ] Write answers of your learning module's "Reflection Publisher-1" questions in this README.
-   **STAGE 2: Implement services and controllers**
    -   [ ] Commit: `Create Notification service struct skeleton.`
    -   [ ] Commit: `Implement subscribe function in Notification service.`
    -   [ ] Commit: `Implement subscribe function in Notification controller.`
    -   [ ] Commit: `Implement unsubscribe function in Notification service.`
    -   [ ] Commit: `Implement unsubscribe function in Notification controller.`
    -   [ ] Write answers of your learning module's "Reflection Publisher-2" questions in this README.
-   **STAGE 3: Implement notification mechanism**
    -   [ ] Commit: `Implement update method in Subscriber model to send notification HTTP requests.`
    -   [ ] Commit: `Implement notify function in Notification service to notify each Subscriber.`
    -   [ ] Commit: `Implement publish function in Program service and Program controller.`
    -   [ ] Commit: `Edit Product service methods to call notify after create/delete.`
    -   [ ] Write answers of your learning module's "Reflection Publisher-3" questions in this README.

## Your Reflections
This is the place for you to write reflections:

### Mandatory (Publisher) Reflections

#### Reflection Publisher-1
##### 1. In the Observer pattern diagram explained by the Head First Design Pattern book, Subscriber is defined as an interface. Explain based on your understanding of Observer design patterns, do we still need an interface (or trait in Rust) in this BambangShop case, or a single Model struct is enough? 

Yes, this single Model struct is enough for our current case. Let me explain. In the Observer pattern diagram written in the classic book, the Subject (observable) has a list of its observers (subscribers) and notifies them of any state changes that each one of them needs to know. The Observer in the book is defined as an interface (which is similar to trait in Rust) and implies that there is a notification method that all the distinct concrete observers have to implement. In our Rust project, we define Subscriber as a struct and this is sufficient in my opinion because we only need the Subscriber to only hold some data in url and name and not any method or behavior that need to be implemented concretely by observers. Furthermore, our Bambangshop project here doesn't really need different types of subscribers with different behaviors at least for the time being, so using trait (in Rust) or interface (in Java) may be unneccessary.

##### 2. id in Program and url in Subscriber is intended to be unique. Explain based on your understanding, is using Vec (list) sufficient or using DashMap (map/dictionary) like we currently use is necessary for this case? 

Well, based on my understanding, url and id can be used as unique identifiers. Using a Vec implies iterating through the list whenever we need to know that a new subscriber is added or an old subscriber is deleted, which should give the time complexity of O(n). Meanwhile, DashMap is a thread-safe implementation of HashMap in Rust which allows for efficient lookup and insertion based on unique identifier url for our key, which should give the time complexity on average of O(1) for insertion, deletion, and lookup, with reasonable assumptions. Since the url is used to identify each one of our subscribers uniquely, it's better to use DashMap over Vec. Not only is DashMap thread-safe, it also provides speed for common operations compared to using Vec in Rust.

##### 3. When programming using Rust, we are enforced by rigorous compiler constraints to make a thread-safe program. In the case of the List of Subscribers (SUBSCRIBERS) static variable, we used the DashMap external library for thread safe HashMap. Explain based on your understanding of design patterns, do we still need DashMap or we can implement Singleton pattern instead?

First of all, we use DashMap and lazy_static! together here because we want to achieve thread safety for our SUBSCRIBERS variable. In our code, lazy_static! implies that the initialization of SUBSCRIBERS happens exactly once in a thread-safe manner. And in the same code, DashMap implies the implementation of some thread-safe HashMap that allows concurrent access to the subscribers. The Singleton pattern in the book is all about controlling the instantiation and ensuring only one instance of a class exists in the program, which is what SUBSCRIBERS manages to almost fulfill due to the lazy_static! macro. Well almost. There are five things that can be noticed here : first, single instance of SUBSCRIBERS is not truly enforced here despite the lazy_static!, second, global access doesn't seem to be the case for our SUBSCRIBERS since it's accessible within the module but not a designated static method nor a globally accessible function, third, lazy initialization of SUBSCRIBERS, fourth, lack of control over instantiation which seem to be what is happening in this code and makes our SUBSCRIBERS not fully singleton in nature because we use lazy_static! macro and not write any private constructor and/or any static method to retrive the single instance, five, the thread safety of SUBSCRIBERS because the data structure DashMap that it uses is multi-threaded concurrent and lock-free, ensuring we have no issue with any thread when multiple threads want to get subscriber data at the same time. Yes, we need DashMap here. And yes, we can implement it as we do not have a true Singleton pattern going on here, BUT we do not really need it in my honest opinion.

#### Reflection Publisher-2

##### 1. In the Model-View Controller (MVC) compound pattern, there is no “Service” and “Repository”. Model in MVC covers both data storage and business logic. Explain based on your understanding of design principles, why do we need to separate “Service” and “Repository” from a Model?
Based on my understanding of design principles , the separation of "Service" and "Repository" from a Model is beneficial in at least two ways. Better maintainability and better testability. Let me explain. 
The first one simply means our Rust code is easier to understand, modify, and extend over time. If the Model is responsible for both business logic and data storage, it violates the Single Responsibility Principle and eventually becomes bloated and complex. Any change to the business logic or data storage can have undesirable effects throughout the Model, making it more difficult to maintain. 
By separating "Service" and "Repository" from the model, we get a clean separation of responsibilities. The model is responsible for representing the core data structure and behavior of the domain, while the Service takes care of the business logic and the Repository takes care of data storage and retrieval. The separation also promotes modularity and reusability unlike not separating.
The second benefit, testability, is related to the separation of concerns. Now we can write more focused and targeted tests for each one of the separated layers. What we mean here is testing the Service where we  want to test the correctness of the buiness logic is independent from testing the Repository where we want to test the correctness of the behaviors specifically related to data persistence, like insert, update, delete operations.And we can still do integration tests to know how correct the interaction between the Service and the Repository are.
##### 2. What happens if we only use the Model? Explain your imagination on how the interactions between each model (Program, Subscriber, Notification) affect the code complexity for each model?

Well, it would increase the complexity for worse. And we can say hello to tight coupling. Each model would have more than one responsibilities, including business logic, data persistence, and interaction with other models, making the code bloated and complex. Also, the models would be very dependant on each other, which means the codebase is harder to maintain. We would most likely have to duplicate a significant amount of codes whenever we want to make a change in any of the Program, Subscriber, or Notification. Overall, using only model would increase the code complexity and make the code less maintainable.

##### 3. Have you explored more about Postman? Tell us how this tool helps you to test your current work. You might want to also list which features in Postman you are interested in or feel like it is helpful to help your Group Project or any of your future software engineering projects.
Yes, I have explored a bit. It has some interesting features that help test our current work. For example, the API Testing is possible in Postman. Postman allows us to send HTTP requests to our API endpoints and view the responses. We can test different HTTP methods (GET, POST, PUT, DELETE, etc.), set request headers, and include request bodies. This helps in validating the functionality and behavior of your API.

And then we can do Postman Collection. Collection in Postman is basically a group of saved requests. Every request we send in Postman appears under the History tab of the sidebar. On a small scale, reusing requests through the history section is convenient. As our Postman usage grows, it can be time-consuming to find a particular request in your history and that's when having a collection is very useful.

There is also the support for Environment Variables in Postman. Postman supports environment variables, which allow people to store and manage different sets of variables for different environments (e.g., development, staging, production). This is useful for easily switching between different configurations without modifying our requests. 

They are all potentially helpful to the Group Project and any of my future software projects. 

#### Reflection Publisher-3

##### 1. Observer Pattern has two variations: Push model (publisher pushes data to subscribers) and Pull model (subscribers pull data from publisher). In this tutorial case, which variation of Observer Pattern that we use?
In this tutorial case, I believe we choose to use the Push model variation of the Observer Pattern. The notify method in our Notification service code pushes the notification payload to each one of the subscribers by calling the update method on the Subscriber. The publisher here, which I assume to be the Notification service, actively sends the data to the subscribers, rather than the subscribers pulling the data from the publisher.
##### 2. What are the advantages and disadvantages of using the other variation of Observer Pattern for this tutorial case? (example: if you answer Q1 with Push, then imagine if we used Pull)
I can say three advantages of using the Pull method. First, subscribers have more control over when they receive updates. They can decide when to pull the data from the publisher based on their own requirements. Second, the publisher does not need to save and keep a list of subscribers, which may potentially reduce the coupling between the publisher and subscribers. Third, subscribers can potentially receive updates on their demand, reducing unnecessary data transfer.

But there are some disadvantages too, obviously. First, the subscribers need to periodically check for updates which can lead to increased complexity and potentially wasted resources if there are no updates available.
Second, the real-time updates could be delayed due to subscribers having to  pull the data rather than receiving notifications. Third, if multiple subscribers pull data too frequently, it can put a very high load on the publisher, especially if the data retrieval procedure is resource-intensive.

##### 3. Explain what will happen to the program if we decide to not use multi-threading in the notification process.

If we don't use multi-threading in the notification process, the notifications would be sent sequentially, one by one. This sequential processing means blocking of the main thread until all subscribers have been notified, meaning longer response times, reduced concurrency, and potential (unwanted) bottlenecks. As the number of subscribers grows or if the notification process becomes time-consuming, the program's overall performance and responsiveness could become really bad.

By using multi-threading, the notification process is handled by multiple separate threads. This lets the main thread continue executing other tasks while the notifications are being sent concurrently. Multi-threading improves the performance and responsiveness of the program by means of concurrent processing, especially when dealing with multiple subscribers and potentially time-consuming notification operations. It helps mitigate the potentially unwanted impact of possible bottlenecks and ensures efficient use of the system resources.